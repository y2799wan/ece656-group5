from client_application import ClientApplication
import datetime
import sys
from decimal import Decimal


def test_accident_lookup():
    client = ClientApplication()
    result = client.accident_lookup("A-1")
    assert result == ('A-1', datetime.timedelta(seconds=2228), datetime.timedelta(seconds=23828), Decimal('40.108909999999995'), Decimal('-83.09286000000000'), Decimal('40.112060000000000'), Decimal('-83.03187000000000'), 'Between Sawmill Rd/Exit 20 and OH-315/Olentangy Riv Rd/Exit 22 - Accident.', 'R', '43017', datetime.datetime(2016, 2, 8, 0, 53), datetime.date(2016, 2, 8), datetime.date(2016, 2, 8), 3, Decimal('3.2300000000000000'), 'Night', 'Night', 'Night', 'Night', None, 'Outerbelt E', 'Dublin', 'Franklin', 'OH', 'US', 'US/Eastern', 'KOSU', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', Decimal('42.1'), Decimal('36.1'), Decimal('58.0'), Decimal('29.76'), Decimal('10.0'), 'SW', Decimal('10.4'), Decimal('0.00'), 'Light Rain')
    
def test_add_new_accident():
    client = ClientApplication()
    inputs = ['ABCD-1', '00:00:01', '00:00:02', '0.000000000000001', '0.000000000000001', '0.000000000000001', '0.000000000000001', 'This is a test- Accident.', 'R', '00001', '2023-04-12 00:00:01', '2023-04-12', '2023-04-12', '3', '0.0100000000000000', 'Night', 'Night', 'Night', 'Night', 1, 'Test E', 'Dublin', 'Franklin', 'OH', 'US', 'US/Eastern', 'KOSU', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', '42.1', '36.1', '58.0', '29.76', '10.0', 'SW', '10.4', '0.00', 'Light Rain']
    input_mock = lambda _: inputs.pop(0)
    client.input = input_mock
    client.add_new_accidents()
    result = client.accident_lookup("ABCD-1")
    assert result == ('ABCD-1', datetime.timedelta(seconds=1), datetime.timedelta(seconds=2), Decimal('0.000000000000001'), Decimal('0.000000000000001'), Decimal('0.000000000000001'), Decimal('0.000000000000001'), 'This is a test- Accident.', 'R', '00001', datetime.datetime(2023, 4, 12, 0, 0, 1), datetime.date(2023, 4, 12), datetime.date(2023, 4, 12), 3, Decimal('0.0100000000000000'), 'Night', 'Night', 'Night', 'Night', 1, 'Test E', 'Dublin', 'Franklin', 'OH', 'US', 'US/Eastern', 'KOSU', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', Decimal('42.1'), Decimal('36.1'), Decimal('58.0'), Decimal('29.76'), Decimal('10.0'), 'SW', Decimal('10.4'), Decimal('0.00'), 'Light Rain')

def test_update_accident_1(monkeypatch):
    client = ClientApplication()
    monkeypatch.setattr('builtins.input', lambda _: 'Start_Time')
    monkeypatch.setattr('builtins.input', lambda _: '00:37:07')
    client.update_accident("A-1")
    result = client.accident_lookup("A-1")
    assert result[1] == datetime.timedelta(seconds=2227)
    sys.stdout = sys.__stdout__

def test_update_accident_2(monkeypatch):
    client = ClientApplication()
    monkeypatch.setattr('builtins.input', lambda _: 'Severity')
    monkeypatch.setattr('builtins.input', lambda _: 2)
    client.update_accident("A-1")
    result = client.accident_lookup("A-1")
    assert result[13] == 2
    sys.stdout = sys.__stdout__
    
def test_update_accident_3(monkeypatch):
    client = ClientApplication()
    monkeypatch.setattr('builtins.input', lambda _: 'Sunrise_Sunset')
    monkeypatch.setattr('builtins.input', lambda _: 'Day')
    client.update_accident("A-1")
    result = client.accident_lookup("A-1")
    assert result[15] == 'Day'
    sys.stdout = sys.__stdout__

def test_update_accident_4(monkeypatch):
    client = ClientApplication()
    monkeypatch.setattr('builtins.input', lambda _: 'Amenity')
    monkeypatch.setattr('builtins.input', lambda _: True)
    client.update_accident("A-1")
    result = client.accident_lookup("A-1")
    assert result[27] == True
    sys.stdout = sys.__stdout__

def test_update_accident_5(monkeypatch):
    client = ClientApplication()
    monkeypatch.setattr('builtins.input', lambda _: 'Start_Lat')
    monkeypatch.setattr('builtins.input', lambda _: 0.000000000000001)
    client.update_accident("A-1")
    result = client.accident_lookup("A-1")
    assert result[3] == 0.000000000000001
    sys.stdout = sys.__stdout__
    
def test_filter_accident_by_severity(monkeypatch):
    client = ClientApplication()
    monkeypatch.setattr('builtins.input', lambda _: '1')
    result = client.filter_accidents_by_severity()
    assert result == None
    
def test_filter_accident_by_location(monkeypatch):
    client = ClientApplication()
    monkeypatch.setattr('builtins.input', lambda _: '40.108909999999995 ')
    monkeypatch.setattr('builtins.input', lambda _: '')
    monkeypatch.setattr('builtins.input', lambda _: '')
    monkeypatch.setattr('builtins.input', lambda _: '')
    monkeypatch.setattr('builtins.input', lambda _: '')
    monkeypatch.setattr('builtins.input', lambda _: '')
    monkeypatch.setattr('builtins.input', lambda _: '')
    monkeypatch.setattr('builtins.input', lambda _: '')
    monkeypatch.setattr('builtins.input', lambda _: '')
    monkeypatch.setattr('builtins.input', lambda _: '')
    monkeypatch.setattr('builtins.input', lambda _: '')
    monkeypatch.setattr('builtins.input', lambda _: '')
    monkeypatch.setattr('builtins.input', lambda _: '')
    result = client.filter_accidents_by_location()
    assert result[0] == "A-1"
    
def test_check_weather_of_accident():
    client = ClientApplication()
    result = client.check_weather_condition_of_accident("A-1")
    assert result[0] == Decimal('42.1')
    
def test_check_surrounding_of_accident():
    client = ClientApplication()
    result = client.check_surrounding_of_accident("A-1")
    assert result[0] == 'False'