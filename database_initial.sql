-- Accident
create table Accident
(
    ID                varchar(10) primary key,
    Start_Time        time,
    End_Time          time,
    Start_Lat         decimal(17, 15),
    Start_Lng         decimal(17, 14),
    End_Lat           decimal(17, 15),
    End_Lng           decimal(17, 14),
    Description       text,
    Side              enum ('R', 'L', 'N'),
    Zipcode           varchar(10),
    Weather_Timestamp datetime,
    Start_Date        date,
    End_Date          date
);

LOAD DATA INFILE '/US_Accidents_Dec21_updated.csv'
    INTO TABLE Accident
    FIELDS TERMINATED BY ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 ROWS
    (ID, @Severity, @csv_start_time, @csv_end_time, Start_Lat, Start_Lng, End_Lat, End_Lng, @Distance, Description,
     @Number, @Street, Side, @City, @County, @State, Zipcode, @Country, @Timezone, @Airport_Code, Weather_Timestamp,
     @Temperature, @Wind_Chill, @Humidity, @Pressure, @Visibility, @Wind_Direction, @Wind_Speed, @Precipitation,
     @Weather_Condition, @Amenity, @Bump, @Crossing, @Give_Way, @Junction, @No_Exit, @Railway, @Roundabout, @Station,
     @Stop, @Traffic_Calming, @Traffic_Signal, @Turning_Loop, @Sunrise_Sunset, @Civil_Twilight, @Nautical_Twilight,
     @Astronomical_Twilight)
    SET
        Start_Time = TIME(@csv_start_time),
        End_Time = TIME(@csv_end_time),
        Start_Date = DATE(@csv_start_time),
        End_Date = DATE(@csv_end_time);

-- Consequence
create table Consequence
(
    ID       varchar(10) primary key,
    Severity int,
    Distance decimal(19, 16)
);

LOAD DATA INFILE '/US_Accidents_Dec21_updated.csv'
    INTO TABLE Consequence
    FIELDS TERMINATED BY ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 ROWS
    (ID, Severity, @Start_Time, @End_Time, @Start_Lat, @Start_Lng, @End_Lat, @End_Lng, Distance, @Description,
     @Number, @Street, @Side, @City, @County, @State, @Zipcode, @Country, @Timezone, @Airport_Code, @Weather_Timestamp,
     @Temperature, @Wind_Chill, @Humidity, @Pressure, @Visibility, @Wind_Direction, @Wind_Speed, @Precipitation,
     @Weather_Condition, @Amenity, @Bump, @Crossing, @Give_Way, @Junction, @No_Exit, @Railway, @Roundabout, @Station,
     @Stop, @Traffic_Calming, @Traffic_Signal, @Turning_Loop, @Sunrise_Sunset, @Civil_Twilight, @Nautical_Twilight,
     @Astronomical_Twilight);

-- Light
create table Light
(
    ID                    varchar(10) primary key,
    Sunrise_Sunset        varchar(5),
    Civil_Twilight        varchar(5),
    Nautical_Twilight     varchar(5),
    Astronomical_Twilight varchar(5)
);

LOAD DATA INFILE '/US_Accidents_Dec21_updated.csv'
    INTO TABLE Light
    FIELDS TERMINATED BY ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 ROWS
    (ID, @Severity, @Start_Time, @End_Time, @Start_Lat, @Start_Lng, @End_Lat, @End_Lng, @Distance, @Description,
     @Number, @Street, @Side, @City, @County, @State, @Zipcode, @Country, @Timezone, @Airport_Code, @Weather_Timestamp,
     @Temperature, @Wind_Chill, @Humidity, @Pressure, @Visibility, @Wind_Direction, @Wind_Speed, @Precipitation,
     @Weather_Condition, @Amenity, @Bump, @Crossing, @Give_Way, @Junction, @No_Exit, @Railway, @Roundabout, @Station,
     @Stop, @Traffic_Calming, @Traffic_Signal, @Turning_Loop, Sunrise_Sunset, Civil_Twilight, Nautical_Twilight,
     Astronomical_Twilight);

-- Location
create table Location
(
    Start_Lat    decimal(17, 15),
    Start_Lng    decimal(17, 14),
    End_Lat      decimal(17, 15),
    End_Lng      decimal(17, 14),
    Number       int,
    Street       varchar(100),
    City         varchar(40),
    County       varchar(30),
    State        char(2),
    Zipcode      varchar(10),
    Country      char(2),
    Timezone     varchar(18),
    Airport_Code char(4)
);

LOAD DATA INFILE '/US_Accidents_Dec21_updated.csv'
    INTO TABLE Location
    FIELDS TERMINATED BY ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 ROWS
    (@ID, @Severity, @Start_Time, @End_Time, Start_Lat, Start_Lng, End_Lat, End_Lng, @Distance, @Description,
     Number, Street, @Side, City, County, State, Zipcode, Country, Timezone, Airport_Code, @Weather_Timestamp,
     @Temperature, @Wind_Chill, @Humidity, @Pressure, @Visibility, @Wind_Direction, @Wind_Speed, @Precipitation,
     @Weather_Condition, @Amenity, @Bump, @Crossing, @Give_Way, @Junction, @No_Exit, @Railway, @Roundabout, @Station,
     @Stop, @Traffic_Calming, @Traffic_Signal, @Turning_Loop, @Sunrise_Sunset, @Civil_Twilight, @Nautical_Twilight,
     @Astronomical_Twilight);

-- Surrounding
create table Surrounding
(
    ID              varchar(10) primary key,
    Amenity         enum ('True', 'False'),
    Bump            enum ('True', 'False'),
    Crossing        enum ('True', 'False'),
    Give_Way        enum ('True', 'False'),
    Junction        enum ('True', 'False'),
    No_Exit         enum ('True', 'False'),
    Railway         enum ('True', 'False'),
    Roundabout      enum ('True', 'False'),
    Station         enum ('True', 'False'),
    Stop            enum ('True', 'False'),
    Traffic_Calming enum ('True', 'False'),
    Traffic_Signal  enum ('True', 'False'),
    Turning_Loop    enum ('True', 'False')
);

LOAD DATA INFILE '/US_Accidents_Dec21_updated.csv'
    INTO TABLE Surrounding
    FIELDS TERMINATED BY ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 ROWS
    (ID, @Severity, @Start_Time, @End_Time, @Start_Lat, @Start_Lng, @End_Lat, @End_Lng, @Distance, @Description,
     @Number, @Street, @Side, @City, @County, @State, @Zipcode, @Country, @Timezone, @Airport_Code, @Weather_Timestamp,
     @Temperature, @Wind_Chill, @Humidity, @Pressure, @Visibility, @Wind_Direction, @Wind_Speed, @Precipitation,
     @Weather_Condition, Amenity, Bump, Crossing, Give_Way, Junction, No_Exit, Railway, Roundabout, Station,
     Stop, Traffic_Calming, Traffic_Signal, Turning_Loop, @Sunrise_Sunset, @Civil_Twilight, @Nautical_Twilight,
     @Astronomical_Twilight);

-- Weather
create table Weather
(
    Start_Lat         decimal(17, 15),
    Start_Lng         decimal(17, 14),
    End_Lat           decimal(17, 15),
    End_Lng           decimal(17, 14),
    Weather_Timestamp datetime,
    Temperature       decimal(4, 1),
    Wind_Chill        decimal(4, 1),
    Humidity          decimal(4, 1),
    Pressure          decimal(4, 2),
    Visibility        decimal(4, 1),
    Wind_Direction    varchar(10),
    Wind_Speed        decimal(5, 1),
    Precipitation     decimal(4, 2),
    Weather_Condition varchar(50)
);

LOAD DATA INFILE '/US_Accidents_Dec21_updated.csv'
    INTO TABLE Weather
    FIELDS TERMINATED BY ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 ROWS
    (@ID, @Severity, @Start_Time, @End_Time, Start_Lat, Start_Lng, End_Lat, End_Lng, @Distance, @Description,
     @Number, @Street, @Side, @City, @County, @State, @Zipcode, @Country, @Timezone, @Airport_Code, Weather_Timestamp,
     Temperature, Wind_Chill, Humidity, Pressure, Visibility, Wind_Direction, Wind_Speed, Precipitation,
     Weather_Condition, @Amenity, @Bump, @Crossing, @Give_Way, @Junction, @No_Exit, @Railway, @Roundabout, @Station,
     @Stop, @Traffic_Calming, @Traffic_Signal, @Turning_Loop, @Sunrise_Sunset, @Civil_Twilight, @Nautical_Twilight,
     @Astronomical_Twilight);

-- Delete duplicate records in Location
CREATE TEMPORARY TABLE Temp_Location
    LIKE Location;

INSERT INTO Temp_Location
SELECT DISTINCT *
FROM Location;

TRUNCATE TABLE Location;

INSERT INTO Location
SELECT *
FROM Temp_Location;

DROP TEMPORARY TABLE Temp_Location;

ALTER TABLE Location
    ADD PRIMARY KEY (Start_Lat, Start_Lng, End_Lat, End_Lng);


-- Delete records with empty weather data in Weather
DELETE
FROM Weather
WHERE Weather_Timestamp IS NULL
  AND Temperature IS NULL
  AND Wind_Chill IS NULL
  AND Humidity IS NULL
  AND Pressure IS NULL
  AND Visibility IS NULL
  AND Wind_Direction IS NULL
  AND Wind_Speed IS NULL
  AND Precipitation IS NULL
  AND Weather_Condition IS NULL;

-- Delete duplicate records in Weather
CREATE TEMPORARY TABLE Temp_Weather
    LIKE Weather;

INSERT INTO Temp_Weather
SELECT DISTINCT *
FROM Weather;

TRUNCATE TABLE Weather;

INSERT INTO Weather
SELECT *
FROM Temp_Weather;

DROP TEMPORARY TABLE Temp_Weather;

ALTER TABLE Weather
    ADD PRIMARY KEY (Start_Lat, Start_Lng, End_Lat, End_Lng, Weather_Timestamp);

ALTER TABLE Consequence
    ADD CONSTRAINT Consequence_Accident_ID_fk
        FOREIGN KEY (ID) REFERENCES Accident (ID);

ALTER TABLE Light
    ADD CONSTRAINT Light_Accident_ID_fk
        FOREIGN KEY (ID) REFERENCES Accident (ID);

ALTER TABLE Surrounding
    ADD CONSTRAINT Surrounding_Accident_ID_fk
        FOREIGN KEY (ID) REFERENCES Accident (ID);
