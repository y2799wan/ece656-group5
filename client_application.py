import pymysql.cursors
import os


class ClientApplication():
    def __init__(self):
      self.connection = pymysql.connect(host="34.130.36.47", user="root",passwd="qwerty123", db="car_accidents")
      self.limit = 5
    
    def change_limit(self, limit):
        self.limit = limit
        
    def accident_lookup(self, id=None):
        while id is None or id.strip() == "":
            id = str(input("Please enter accident ID: "))
        cursor = self.connection.cursor()
        sql = f"""select a.ID, a.Start_Time, a.End_Time, a.Start_Lat, a.Start_Lng, a.End_Lat, a.End_lng, a.Description, 
	              a.Side, a.Zipcode, a.Weather_Timestamp, a.Start_Date, End_Date,
                  c.Severity, c.Distance,
                  li.Sunrise_Sunset, li.Civil_Twilight, li.Nautical_Twilight, li.Astronomical_Twilight,
                  lo.Number as Number, lo.Street, lo.City, lo.County, lo.State, lo.Country, lo.Timezone, lo.Airport_Code,
                  s.Amenity, s.Bump, s.Crossing, s.Give_Way, s.Junction, s.No_Exit, s.Railway, s.Roundabout, s.Station, s.Stop as Stop, s.Traffic_Calming, s.Traffic_Signal, s.Turning_Loop,
                  w.Temperature, w.Wind_Chill, w.Humidity, w.Pressure, w.Visibility, w.Wind_Direction, w.Wind_Speed, w.Precipitation, w.Weather_Condition
                  from Accident a
                  join Consequence c on a.ID = c.ID
                  join Light li on a.ID = li.ID
                  join Location lo on a.Start_Lat = lo.Start_Lat and a.Start_Lng = lo.Start_Lng and a.End_Lat = lo.End_Lat and a.End_Lng = lo.End_Lng
                  join Surrounding s on a.ID = s.ID
                  join Weather w on a.Weather_TimeStamp = w.Weather_TimeStamp and a.Start_Lat = w.Start_Lat
                  where a.ID = "{id}";"""
        cursor.execute(sql)
        row_old = cursor.fetchone()
        print()
        
        row_list = list(row_old)
        for i in range(len(row_list)):
            if row_list[i] is None:
                row_list[i] = 'N/A'
        row = tuple(row_list)
        
        print(f"{'Accident ID':<15} {'Start_Time':<10} {'End_Time':<10} {'Start_Lat':<18} {'Start_Lng':<20} {'End_Lat':<20} {'End_lng':<20} {'Description':<20}")
        print(f"{row[0]:<15} {str(row[1]):<10} {str(row[2]):<10} {row[3]:<10} {row[4]:<20} {row[5]:<20} {row[6]:<20} {row[7]:<20} \n")
        
        print(f"{'Side':<2} {'Zipcode':<6} {'Weather_Timestamp':<22} {'Start_Date':<12} {'End_Date':<12} {'Severity':<10} {'Traffic Jam Distance (miles)':<20}")
        print(f"{row[8]:<4} {row[9]:<7} {row[10]}    {str(row[11]):<12} {str(row[12]):<12} {str(row[13]):<10} {row[14]:<10}  \n")
        
        print(f"{'Sunrise_Sunset':<15} {'Civil_Twilight':<15} {'Nautical_Twilight':<20} {'Astronomical_Twilight':<20}")
        print(f"{row[15]:<15} {str(row[16]):<15} {str(row[17]):<20} {row[18]:<20} \n")
        
        print(f"{'Number':<5} {'Street':<15} {'City':<15} {'County':<20} {'State':<5} {'Country':<3} {'Timezone':<10} {'Airport_Code':<5}")
        print(f"{row[19]:<6} {row[20]:<15} {row[21]:<15} {row[22]:<20} {row[23]:<5} {row[24]:<7} {row[25]:<3} {row[26]:<3}\n")
        
        print(f"{'Amenity':<5} {'Bump':<5} {'Crossing':<5} {'Give_Way':<5} {'Junction':<5} {'No_Exit':<5} {'Railway':<5} {'Roundabout':<5} {'Station':<5} {'Stop':<7} {'Traffic_Calming':<5} {'Traffic_Signal':<5} {'Turning_Loop':<5}")
        print(f"{row[27]:<7} {row[28]:<5} {row[29]:<8} {row[30]:<8} {row[31]:<8} {row[32]:<7} {row[33]:<7} {row[34]:<10} {row[35]:<7} {row[36]:<7} {row[37]:<15} {row[38]:<14} {row[39]:<7} \n")
        
        print(f"{'Temperature':<5} {'Wind_Chill':<5} {'Humidity':<5} {'Pressure':<5} {'Visibility':<5} {'Wind_Direction':<5} {'Wind_Speed':<5} {'Precipitation':<5} {'Weather_Condition':<5}")
        print(f"{row[40]:<11} {row[41]:<10} {row[42]:<8} {row[43]:<8} {row[44]:<10} {row[45]:<14} {row[46]:<10} {row[47]:<13} {row[48]:<12}\n")
        return row_old


    def add_new_accidents(self):
        print("For the information you are about to enter, please hit enter (leave the input as empty) if the information is not available.")
        id = None
        while id is None or id.strip() == "":
            id = input("Please enter accident ID: ")
        
        Severity = input("Please enter Severity") or None
        Start_Date = input("Please enter Start_Time") or None
        End_Date = input("Please enter End_Time") or None
        Start_Time = input("Please enter Start_Time") or None
        End_Time = input("Please enter End_Time") or None
        Start_Lat = input("Please enter Start_Lat") or None
        Start_Lng = input("Please enter Start_Lng") or None
        End_Lat = input("Please enter End_Lat") or None
        End_Lng = input("Please enter End_Lng") or None
        Distance = input("Please enter Distance") or None
        Description = input("Please enter Description") or None
        Number = input("Please enter Number") or None
        Street = input("Please enter Street") or None
        Side = input("Please enter Side") or None
        City = input("Please enter City") or None
        County = input("Please enter County") or None
        State = input("Please enter State") or None
        Zipcode = input("Please enter Zipcode") or None
        Country = input("Please enter Country") or None
        Timezone = input("Please enter Timezone") or None
        Airport_Code = input("Please enter Airport_Code") or None
        Weather_Timestamp = input("Please enter Weather_Timestamp") or None
        Temperature = input("Please enter Temperature") or None
        Wind_Chill = input("Please enter Wind_Chill") or None
        Humidity = input("Please enter Humidity") or None
        Pressure = input("Please enter Pressure") or None
        Visibility = input("Please enter Visibility") or None
        Wind_Direction = input("Please enter Wind_Direction") or None
        Wind_Speed = input("Please enter Wind_Speed") or None
        Precipitation = input("Please enter Precipitation") or None
        Weather_Condition = input("Please enter Weather_Condition") or None
        Amenity = input("Please enter Amenity") or None
        Bump = input("Please enter Bump") or None
        Crossing = input("Please enter Crossing") or None
        Give_Way = input("Please enter Give_Way") or None
        Junction = input("Please enter Junction") or None
        No_Exit = input("Please enter No_Exit") or None
        Railway = input("Please enter Railway") or None
        Roundabout = input("Please enter Roundabout") or None
        Station = input("Please enter Station") or None
        Stop = input("Please enter Stop") or None
        Traffic_Calming = input("Please enter Traffic_Calming") or None
        Traffic_Signal = input("Please enter Traffic_Signal") or None
        Turning_Loop = input("Please enter Turning_Loop") or None
        Sunrise_Sunset = input("Please enter Sunrise_Sunset") or None
        Civil_Twilight = input("Please enter Civil_Twilight") or None
        Nautical_Twilight = input("Please enter Nautical_Twilight") or None
        Astronomical_Twilight = input("Please enter Astronomical_Twilight") or None
        
        cursor = self.connection.cursor()
        sql = f"""insert into Accident (ID, Start_Time, End_Time, Start_Lat, Start_Lng, End_Lat, End_Lng, Description, Side, Zipcode, Weather_Timestamp, Start_Date, End_Date)
                              values ({id}, {Start_Time}, {End_Time}, {Start_Lat}, {Start_Lng}, {End_Lat}, {End_Lng}, {Description}, {Side}, {Zipcode}, {Weather_Timestamp}, {Start_Date}, {End_Date});"""
        cursor.execute(sql)
        
        sql = f"""insert into Consequence (ID, Severity, Distance)
                              values ({id}, {Severity}, {Distance})"""
        cursor.execute(sql)
        
        sql = f"""insert into Light (ID, Sunrise_Sunset, Civil_Twilight, Nautical_Twilight, Astronomical_Twilight)
                              values ({id}, {Sunrise_Sunset}, {Civil_Twilight}, {Nautical_Twilight}, {Astronomical_Twilight});"""
        cursor.execute(sql)
        
        sql = f"""insert into Location (Start_Lat, Start_Lng, End_Lat, End_Lng, Number, Street, City, County, State, Zipcode, Country, Timezone, Airport_Code)
                              values ({Start_Lat}, {Start_Lng}, {End_Lat}, {End_Lng}, {Number}, {Street}, {City}, {County}, {State}, {Zipcode}, {Country}, {Timezone}, {Airport_Code});"""
        cursor.execute(sql)
        
        sql = f"""insert into Surrounding (ID, Amenity, Bump, Crossing, Give_Way, Junction, No_Exit, Railway, Roundabout, Station, Stop, Traffic_Calming, Traffic_Signal, Turning_Loop)
                              values ({id}, {Amenity}, {Bump}, {Crossing}, {Give_Way}, {Junction}, {No_Exit}, {Railway}, {Roundabout}, {Station}, {Stop}, {Traffic_Calming}, {Traffic_Signal}, {Turning_Loop});"""
        cursor.execute(sql)
        
        sql = f"""insert into Weather (Start_Lat, Start_Lng, End_Lat, End_Lng, Weather_Timestamp, Temperature, Wind_Chill, Humidity, Pressure, Visibility, Wind_Direction, Wind_Speed, Precipitation, Weather_Condition)
                              values ({Start_Lat}, {Start_Lng}, {End_Lat}, {End_Lng}, {Weather_Timestamp}, {Temperature}, {Wind_Chill}, {Humidity}, {Pressure}, {Visibility}, {Wind_Direction}, {Wind_Speed}, {Precipitation}, {Weather_Condition});"""
        cursor.execute(sql)

     
    def update_accident(self, id=None):
        cursor = self.connection.cursor()
        while id is None or id.strip() == "":
            id = input("Please enter accident ID: ")
            
        print("This is the current information on the accident ID you provided:")
        self.accident_lookup(id)
            
        item = input("Please enter the information you want to update: ")
        if item in ["Start_Time", "End_Time", "Description", "Side", "Zipcode", "Weather_Timestamp", "Start_Date", "End_Date"]:
            sql = f"update Accident set {item} = '{input('Please enter the new value: ')}' where ID = '{id}'"
            print(sql)
            cursor.execute(sql)
            
        if item in ["Severity", "Distance"]:
            sql = f"update Consequence set {item} = '{input('Please enter the new value: ')}' where ID = '{id}'"
            cursor.execute(sql)
            
        if item in ["Sunrise_Sunset", "Civil_Twilight", "Nautical_Twilight", "Astronomical_Twilight"]:
            sql = f"update Light set {item} = '{input('Please enter the new value: ')}' where ID = '{id}'"
            cursor.execute(sql)
            
        if item in ["Amenity", "Bump", "Crossing", "Give_Way", "Junction", "No_Exit", "Railway", "Roundabout", "Station", "Stop", "Traffic_Calming", "Traffic_Signal", "Turning_Loop"]:
            sql = f"update Surrounding set {item} = '{input('Please enter the new value: ')}' where ID = '{id}'"
            cursor.execute(sql)

        if item in ["Start_Lat", "Start_Lng", "End_Lat", "End_Lng"]:
            new_value = input('Please enter the new value: ')
            sql = f"update Accident set '{item}' = {new_value} where ID = '{id}'"
            cursor.execute(sql)
            
            sql = f"""select a.Start_Lat, a.Start_Lng, a.End_Lat, a.End_Lng,
                  lo.Number as Number, lo.Street, lo.City, lo.County, lo.State, lo.Country, lo.Timezone, lo.Airport_Code,
                  w.Temperature, w.Wind_Chill, w.Humidity, w.Pressure, w.Visibility, w.Wind_Direction, w.Wind_Speed, w.Precipitation, w.Weather_Condition
                  from Accident a
                  join Location lo on a.Start_Lat = lo.Start_Lat and a.Start_Lng = lo.Start_Lng and a.End_Lat = lo.End_Lat and a.End_Lng = lo.End_Lng
                  join Weather w on a.Weather_TimeStamp = w.Weather_TimeStamp and a.Start_Lat = w.Start_Lat
                  where a.ID = "{id}";"""
            cursor.execute(sql)
            row = cursor.fetchone()
            Weather_Timestamp, Zipcode, Start_Lat, Start_Lng, End_Lat, End_Lng, Number, Street, City, County, State, Country, Timezone, Airport_Code, Temperature, Wind_Chill, Humidity, Pressure, Visibility, Wind_Direction, Wind_Speed, Precipitation, Weather_Condition = row
            if item == "Start_Lat":
                Start_Lat = new_value
            elif item == "Start_Lng":
                Start_Lng = new_value
            elif item == "End_Lat":
                End_Lat = new_value
            elif item == "End_Lng":
                End_Lng = new_value
            
            sql = f"""insert into Location (Start_Lat, Start_Lng, End_Lat, End_Lng, Number, Street, City, County, State, Zipcode, Country, Timezone, Airport_Code)
                              values ({Start_Lat}, {Start_Lng}, {End_Lat}, {End_Lng}, {Number}, {Street}, {City}, {County}, {State}, {Zipcode}, {Country}, {Timezone}, {Airport_Code});"""
            cursor.execute(sql)
            
            sql = f"""insert into Weather (Start_Lat, Start_Lng, End_Lat, End_Lng, Weather_Timestamp, Temperature, Wind_Chill, Humidity, Pressure, Visibility, Wind_Direction, Wind_Speed, Precipitation, Weather_Condition)
                              values ({Start_Lat}, {Start_Lng}, {End_Lat}, {End_Lng}, {Weather_Timestamp}, {Temperature}, {Wind_Chill}, {Humidity}, {Pressure}, {Visibility}, {Wind_Direction}, {Wind_Speed}, {Precipitation}, {Weather_Condition});"""
            cursor.execute(sql)


    def filter_accidents_by_severity(self):
        severity_level = input("Enter severity level (1-4): ")
        if severity_level not in ["1", "2", "3", "4"]:
            print("Invalid severity level.")
            return
        cursor = self.connection.cursor()
        sql = f"select * from Consequence where `Severity` = {severity_level} limit {self.limit}"
        cursor.execute(sql) 
        print(f"{'Accident ID':<15} {'Severity':<10} {'Traffic Jam Distance (miles)':<25}")
        row = cursor.fetchone()
        while row:
            print(f"{row[0]:<15} {row[1]:<10} {str(round(row[2], 5)):<8}")
            row = cursor.fetchone()
        print(row)
        return row


    def search_accidents_by_location(self):
        cursor = self.connection.cursor()
        print("Please enter the location information you want to search by. Leave blank if you don't want to search by that field.")
        
        Start_Lat = input("Please enter Start_Lat: ") or None
        Start_Lng = input("Please enter Start_Lng: ") or None
        End_Lat = input("Please enter End_Lat: ") or None
        End_Lng = input("Please enter End_Lng: ") or None
        Number = input("Please enter Number: ") or None
        Street = input("Please enter Street: ") or None
        City = input("Please enter City: ") or None
        County = input("Please enter County: ") or None
        State = input("Please enter State: ") or None
        Zipcode = input("Please enter Zipcode: ") or None
        Country = input("Please enter Country: ") or None
        Timezone = input("Please enter Timezone: ") or None
        Airport_Code = input("Please enter Airport_Code: ") or None
        
        where_clause = ""
        if Start_Lat:
            where_clause += f"lo.Start_Lat = {Start_Lat} and "
        if Start_Lng:
            where_clause += f"lo.Start_Lng = {Start_Lng} and "
        if End_Lat:
            where_clause += f"lo.End_Lat = {End_Lat} and "
        if End_Lng:
            where_clause += f"lo.End_Lng = {End_Lng} and "
        if Number:
            where_clause += f"lo.Number = {Number} and "
        if Street:
            where_clause += f"lo.Street = {Street} and "
        if City:
            where_clause += f"lo.City = {City} and "
        if County:
            where_clause += f"lo.County = {County} and "
        if State:
            where_clause += f"lo.State = {State} and "
        if Zipcode:
            where_clause += f"lo.Zipcode = {Zipcode} and "
        if Country:
            where_clause += f"lo.Country = {Country} and "
        if Timezone:
            where_clause += f"lo.Timezone = {Timezone} and "
        if Airport_Code:
            where_clause += f"lo.Airport_Code = {Airport_Code} and "
        
        where_clause = where_clause[:-4]
        
        sql = f"""select a.ID, a.Start_Time, a.End_Time, a.Start_Lat, a.Start_Lng, a.End_Lat, a.End_lng, a.Description, 
	              a.Side, a.Zipcode, a.Weather_Timestamp, a.Start_Date, End_Date,
                  c.Severity, c.Distance,
                  li.Sunrise_Sunset, li.Civil_Twilight, li.Nautical_Twilight, li.Astronomical_Twilight,
                  lo.Number as Number, lo.Street, lo.City, lo.County, lo.State, lo.Country, lo.Timezone, lo.Airport_Code,
                  s.Amenity, s.Bump, s.Crossing, s.Give_Way, s.Junction, s.No_Exit, s.Railway, s.Roundabout, s.Station, s.Stop as Stop, s.Traffic_Calming, s.Traffic_Signal, s.Turning_Loop,
                  w.Temperature, w.Wind_Chill, w.Humidity, w.Pressure, w.Visibility, w.Wind_Direction, w.Wind_Speed, w.Precipitation, w.Weather_Condition
                  from Accident a
                  join Consequence c on a.ID = c.ID
                  join Light li on a.ID = li.ID
                  join Location lo on a.Start_Lat = lo.Start_Lat and a.Start_Lng = lo.Start_Lng and a.End_Lat = lo.End_Lat and a.End_Lng = lo.End_Lng
                  join Surrounding s on a.ID = s.ID
                  join Weather w on a.Weather_TimeStamp = w.Weather_TimeStamp and a.Start_Lat = w.Start_Lat
                  where {where_clause}
                  limit {self.limit};"""
        cursor.execute(sql)
        row = cursor.fetchone()
        print()
        
        row_list = list(row)
        for i in range(len(row_list)):
            if row_list[i] is None:
                row_list[i] = 'N/A'
        row = tuple(row_list)
        
        print(f"{'Accident ID':<15} {'Start_Time':<10} {'End_Time':<10} {'Start_Lat':<18} {'Start_Lng':<20} {'End_Lat':<20} {'End_lng':<20} {'Description':<20}")
        print(f"{row[0]:<15} {str(row[1]):<10} {str(row[2]):<10} {row[3]:<10} {row[4]:<20} {row[5]:<20} {row[6]:<20} {row[7]:<20} \n")
        
        print(f"{'Side':<2} {'Zipcode':<6} {'Weather_Timestamp':<22} {'Start_Date':<12} {'End_Date':<12} {'Severity':<10} {'Traffic Jam Distance (miles)':<20}")
        print(f"{row[8]:<4} {row[9]:<7} {row[10]}    {str(row[11]):<12} {str(row[12]):<12} {str(row[13]):<10} {row[14]:<10}  \n")
        
        print(f"{'Sunrise_Sunset':<15} {'Civil_Twilight':<15} {'Nautical_Twilight':<20} {'Astronomical_Twilight':<20}")
        print(f"{row[15]:<15} {str(row[16]):<15} {str(row[17]):<20} {row[18]:<20} \n")
        
        print(f"{'Number':<5} {'Street':<15} {'City':<15} {'County':<20} {'State':<5} {'Country':<3} {'Timezone':<10} {'Airport_Code':<5}")
        print(f"{row[19]:<6} {row[20]:<15} {row[21]:<15} {row[22]:<20} {row[23]:<5} {row[24]:<7} {row[25]:<3} {row[26]:<3}\n")
        
        print(f"{'Amenity':<5} {'Bump':<5} {'Crossing':<5} {'Give_Way':<5} {'Junction':<5} {'No_Exit':<5} {'Railway':<5} {'Roundabout':<5} {'Station':<5} {'Stop':<7} {'Traffic_Calming':<5} {'Traffic_Signal':<5} {'Turning_Loop':<5}")
        print(f"{row[27]:<7} {row[28]:<5} {row[29]:<8} {row[30]:<8} {row[31]:<8} {row[32]:<7} {row[33]:<7} {row[34]:<10} {row[35]:<7} {row[36]:<7} {row[37]:<15} {row[38]:<14} {row[39]:<7} \n")
        
        print(f"{'Temperature':<5} {'Wind_Chill':<5} {'Humidity':<5} {'Pressure':<5} {'Visibility':<5} {'Wind_Direction':<5} {'Wind_Speed':<5} {'Precipitation':<5} {'Weather_Condition':<5}")
        print(f"{row[40]:<11} {row[41]:<10} {row[42]:<8} {row[43]:<8} {row[44]:<10} {row[45]:<14} {row[46]:<10} {row[47]:<13} {row[48]:<12}\n")

        return row

    
    def check_weather_condition_of_accident(self, id=None):
        while id is None or id.strip() == "":
            id = input("Please enter accident ID: ")
        cursor = self.connection.cursor()
        sql = f"""select w.Temperature, w.Wind_Chill, w.Humidity, w.Pressure, w.Visibility, w.Wind_Direction, w.Wind_Speed, w.Precipitation, w.Weather_Condition
                  from Accident a
                  join Weather w on a.Weather_TimeStamp = w.Weather_TimeStamp and a.Start_Lat = w.Start_Lat
                  where a.ID = "{id}";"""
        cursor.execute(sql)
        row = cursor.fetchone()
        print()
        
        row_list = list(row)
        for i in range(len(row_list)):
            if row_list[i] is None:
                row_list[i] = 'N/A'
        row = tuple(row_list)
        
        print(f"{'Temperature':<5} {'Wind_Chill':<5} {'Humidity':<5} {'Pressure':<5} {'Visibility':<5} {'Wind_Direction':<5} {'Wind_Speed':<5} {'Precipitation':<5} {'Weather_Condition':<5}")
        print(f"{row[0]:<11} {row[1]:<10} {row[2]:<8} {row[3]:<8} {row[4]:<10} {row[5]:<14} {row[6]:<10} {row[7]:<13} {row[8]:<12}\n")
        
        return row


    def check_surrounding_of_accident(self, id=None):
        while id is None or id.strip() == "":
            id = input("Please enter accident ID: ")
        cursor = self.connection.cursor()
        sql = f"""select s.Amenity, s.Bump, s.Crossing, s.Give_Way, s.Junction, s.No_Exit, s.Railway, s.Roundabout, s.Station, s.Stop as Stop, s.Traffic_Calming, s.Traffic_Signal, s.Turning_Loop
                  from Accident a
                  join Surrounding s on a.ID = s.ID
                  where a.ID = "{id}";"""
        cursor.execute(sql)
        row = cursor.fetchone()
        print()
        
        row_list = list(row)
        for i in range(len(row_list)):
            if row_list[i] is None:
                row_list[i] = 'N/A'
        row = tuple(row_list)
        
        print(f"{'Amenity':<5} {'Bump':<5} {'Crossing':<5} {'Give_Way':<5} {'Junction':<5} {'No_Exit':<5} {'Railway':<5} {'Roundabout':<5} {'Station':<5} {'Stop':<7} {'Traffic_Calming':<5} {'Traffic_Signal':<5} {'Turning_Loop':<5}")
        print(f"{row[0]:<7} {row[1]:<5} {row[2]:<8} {row[3]:<8} {row[4]:<8} {row[5]:<7} {row[6]:<7} {row[7]:<10} {row[8]:<7} {row[9]:<7} {row[10]:<15} {row[11]:<14} {row[12]:<7} \n")
        return row
    
def main():
    client_app = ClientApplication()
    while True:
        # prompt user for functionality
        print("Please select a functionality:")
        print("1. Accident lookup")
        print("2. Add new accidents")
        print("3. Update existing accidents")
        print("4. Filter accidents by severity level")
        print("5. Search for accidents by location")
        print("6. Check weather conditions during an accident")
        print("7. Check surroundings of an accident")
        print("8. Change number of rows to display from query (LIMIT)")
        print("9. Exit Program")
        choice = input("Enter your choice (1-9): ")
        
        # handle user's choice
        if choice == "1":
            client_app.accident_lookup()
        elif choice == "2":
            client_app.add_new_accidents()
        elif choice == "3":
            client_app.update_accident()
        elif choice == "4":
            client_app.filter_accidents_by_severity()
        elif choice == "5":
            client_app.search_accidents_by_location()
        elif choice == "6":
            client_app.check_weather_condition_of_accident()
        elif choice == "7":
            client_app.check_surrounding_of_accident()
        elif choice == "8":
            limit = input("Enter new limit (an integer greater than 0): ")
            client_app.change_limit(limit)
        elif choice == "9":
            client_app.connection.close()
            break
        else:
            print("Invalid choice.")
            continue
        
        # ask user if they want to continue or quit
        print()
        print()
        choice = input("Do you want to do another search? (y/n): ")
        if choice.lower() == "n":
            client_app.connection.close()
            break
        else:
            os.system('clear')
    

if __name__ == "__main__":
    main()