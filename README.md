# ECE656-Group5
Qinyi Xu & Yiwei Wang

## File Breakdown
- US_Accidents_Dec21_updated.csv: This is the dataset we used for this project. [Source](https://www.kaggle.com/datasets/sobhanmoosavi/us-accidents)
- client_application: The client application developed for users. Some features include adding, updating and looking up details of accidents in the database.
- database_initial:  A SQL script to load data into the database.
- data_mining.ipynb: A Jupyter Notebook that is created for the data mining exercise and data visualization